import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {labels: []};
  }

  componentDidMount() {

    var self = this;

    setInterval(function() {
        var url = "http://onboardingapp-env.4ngrxbf44x.eu-central-1.elasticbeanstalk.com/query/message";
        fetch(url)
          .then(response => {
            return response.json();
          })
          .then(d => {
            if (self.state.labels.length === 0) {

                d.forEach((item) => {
                    item.style = self.getStyle();
                });

                self.setState({ labels: d });
            }
            else {
                d.forEach((item) => {

                    var found = false;
                    self.state.labels.forEach((label) => {

                        if (item.label === label.label) {
                            found = true;
                        }
                    });

                    if (!found) {

                        item.style = self.getStyle();

                        self.state.labels.push(item)
                        self.setState(self.state);
                    }
                });
            }

          })
          .catch(error => console.log(error))

    }, 5000 );
  }

  render() {
      const labels = this.state.labels.map((item, i) => (

        <div style={item.style} className="App-text">
          <h1>{ item.label }</h1>
        </div>
      ));

      return (
        <div id="layout-content" className="layout-content-wrapper">
          <div className="App-header">{ labels }</div>
        </div>
      );
  }

  getStyle() {
    var divsize = ((Math.random()*100) + 50).toFixed();

    var posx = (Math.random() * (window.innerWidth - divsize)).toFixed();
    var posy = (Math.random() * (window.innerHeight - divsize)).toFixed();

    var color = "white";
    var width = divsize + "px";
    var height = divsize + "px";
    var position = "absolute";
    var display = "block";
    var left = posx + "px";
    var top = posy + "px";

    return  {
      width: width,
      height: height,
      position: position,
      color: color,
      display: display,
      left: left,
      top: top
    };
  }
}

export default App;
